package be.kdg.sa.RideService;

import be.kdg.sa.RideService.model.Lock;
import be.kdg.sa.RideService.services.LockService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


@RunWith(SpringRunner.class)
@SpringBootTest
public class LockServiceTest {
    @Autowired
    private LockService lockService;
    private final Logger LOGGER = Logger.getLogger(LockServiceTest.class.getName());

    @Test
    public void findAllFreeLockByStation(){
        List<Long> locks = lockService.findAllFreeLocksByStation((long) 1);
        List<Long> freelocks = new ArrayList<>();
        for (Long lock : locks) {
            LOGGER.info("A free lock was found for the station : " + lock.toString());
            freelocks.add(lock);
        }
        Assert.notEmpty(freelocks, "");
    }

    @Test
    public void findAll(){
        List<Lock> locks = lockService.findAll();
        Assert.notEmpty(locks, "locklist");
    }
}
