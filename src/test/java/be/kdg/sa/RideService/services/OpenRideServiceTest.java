package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.model.Ride;
import be.kdg.sa.RideService.model.messages.LocationMessage;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OpenRideServiceTest {

    @Autowired
    private OpenRideService openRideService;
    @Autowired
    private RideService rideService;
    @Autowired
    private LocationMessageService locationMessageService;
    private Ride ride;
    private LocationMessage locationMessage;
    private LocationMessage lastLocationMessage;


    @PostConstruct
    @Transactional
    public void createOpenTimeRide() {
        ride = new Ride(LocalDateTime.now().minusHours(6L), 1L,1L);
        rideService.save(ride);
        locationMessage = new LocationMessage(1L, 4.25, 51.47, LocalDateTime.now().minusHours(1L));
        locationMessageService.save(locationMessage);
        lastLocationMessage = new LocationMessage(1L, 4.25, 51.47, LocalDateTime.now());
        locationMessageService.save(lastLocationMessage);
    }

    @Test
    public void testOpenRideDetection() {
        Assert.assertTrue(openRideService.checkOpenRidesByTime());
    }

    @Test
    public void testOpenRideLocation() {
        Assert.assertTrue(openRideService.checkOpenRidesLocation());
    }

}
