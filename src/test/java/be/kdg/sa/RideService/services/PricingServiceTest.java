package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.exceptions.VehicleNotFoundException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.logging.Logger;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PricingServiceTest {
    @Autowired
    private PricingService pricingService;
    private static Logger LOGGER = Logger.getLogger(PricingServiceTest.class.getName());

    @Test
    public void calculatePrice() throws VehicleNotFoundException, IOException {
        double price =  pricingService.calculatePrice(1L, 1L);
        LOGGER.info("The price of the ride is: " + price);
        Assert.assertEquals(0.0, price, 0.0);
    }

}
