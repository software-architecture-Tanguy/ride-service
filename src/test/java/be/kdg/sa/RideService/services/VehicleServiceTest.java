package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.exceptions.VehicleNotFoundException;
import be.kdg.sa.RideService.model.Vehicle;
import be.kdg.sa.RideService.model.dto.UpdateVehicleDTO;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.List;
import java.util.logging.Logger;


@RunWith(SpringRunner.class)
@SpringBootTest
public class VehicleServiceTest {
    @Autowired
    private VehicleService vehicleService;
    private final Logger LOGGER = Logger.getLogger(VehicleServiceTest.class.getName());
    private Vehicle vehicle;

    @Test
    public void findAllVehiclesTest(){
        List<Vehicle> vehicles = vehicleService.findAllVehicles();
        for (Vehicle vehicle : vehicles) {
            LOGGER.info("A vehicle was found: " + vehicle.toString());
        }
        Assert.assertFalse(vehicles.isEmpty());
    }

    @PostConstruct
    @Test
    public void createNewVehicle() {
        vehicle = new Vehicle();
        GeometryFactory gf = new GeometryFactory();
        Point point = gf.createPoint(new Coordinate(0.6,0.2));
        vehicle.setBikeLotId((long) 1);
        vehicle.setLastMaintenanceOn(LocalDateTime.now());
        vehicle.setLockId((long) 1);
        vehicle.setSerialNumber(String.valueOf(1000000));
        vehicle.setPoint(point);
        Assert.assertEquals(vehicleService.save(vehicle), vehicle);
        LOGGER.info("Vehicle was saved!");

    }

    @Test(expected = VehicleNotFoundException.class)
    public void deleteVehicle() throws VehicleNotFoundException {
        vehicleService.delete((long) 7001);
        LOGGER.info("Vehicle has been deleted!");
        Assert.assertNull(vehicleService.load(7001L));
    }

    @Test
    public void updateVehicle() throws VehicleNotFoundException {
        UpdateVehicleDTO updateVehicleDTO = new UpdateVehicleDTO();
        updateVehicleDTO.setLastMaintenanceOn(LocalDateTime.now());
        updateVehicleDTO.setBikeLotId((long) 1);
        updateVehicleDTO.setLockId((long) 1);
        updateVehicleDTO.setSerialNumber("999999");
        GeometryFactory gf = new GeometryFactory();
        Point point = gf.createPoint(new Coordinate(9.2,4.2));
        updateVehicleDTO.setPoint(point);
        vehicleService.updateVehicle(vehicle.getVehicleId(), updateVehicleDTO);
        Assert.assertEquals(vehicleService.load(vehicle.getVehicleId()).getSerialNumber(), "999999");
    }
}
