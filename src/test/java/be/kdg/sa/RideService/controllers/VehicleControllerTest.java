package be.kdg.sa.RideService.controllers;

import be.kdg.sa.RideService.model.Vehicle;
import be.kdg.sa.RideService.model.dto.UpdateVehicleDTO;
import be.kdg.sa.RideService.services.VehicleService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.LocalDateTime;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class VehicleControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private VehicleService vehicleService;
    private Vehicle vehicle;


    @PostConstruct
    @Transactional
    public void createTestVehicle() {
        GeometryFactory gf = new GeometryFactory();
        Point point = gf.createPoint(new Coordinate(15.2,78.2));
        vehicle = vehicleService.save(new Vehicle("testnr", 1L,LocalDateTime.now(), 1L, point));
    }


    @Test
    @Transactional
    public void updateVehicle() throws Exception {
        GeometryFactory gf = new GeometryFactory();
        Point point = gf.createPoint(new Coordinate(15.2,78.2));
        UpdateVehicleDTO updateVehicleDTO = new UpdateVehicleDTO("updatednr", 12000L, LocalDateTime.now(), 15000L, point );
        String request = objectMapper.writeValueAsString(updateVehicleDTO);

        mockMvc.perform(put("/vehicle/update/" + vehicle.getVehicleId() + "")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(request))
                .andDo(print())
                .andExpect(status().isAccepted())
                .andExpect(content()
                        .string(containsString("updatednr")));
    }


    @Test
    @Transactional
    public void loadVehicle() throws Exception {
        mockMvc.perform(get("/vehicle/load/" + vehicle.getVehicleId() + "")).andDo(print()).andExpect(status().isOk())
                .andExpect(content()
                        .string(containsString("testnr")));
    }


    @Test
    @Transactional
    public void deleteVehicle() throws Exception {
        mockMvc.perform(delete("/vehicle/delete/" + vehicle.getVehicleId())).andDo(print())
                .andExpect(status().isNoContent());
    }


}
