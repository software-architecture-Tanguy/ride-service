package be.kdg.sa.RideService.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class JSONService {

    public Object convertJSONToObject(String jsonstring, Class classobject) {
        Object object = null;
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        try {
            object = objectMapper.readValue(jsonstring, classobject);
        } catch (IOException e) {
            log.warn("An IO exception occured during parsing. Please check your paths.");
            e.printStackTrace();
        }
        return object;
    }
}
