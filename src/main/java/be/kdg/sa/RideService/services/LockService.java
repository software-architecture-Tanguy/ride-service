package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.model.Lock;
import be.kdg.sa.RideService.persistence.LockRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class LockService {
    private final LockRepository lockRepository;

    public List<Long> findAllFreeLocksByStation(Long stationId) {
        List<Lock> locks = lockRepository.findAllByStationIdAndVehicleIdIsNotNull(stationId);
        List<Long> lockIds = new ArrayList<>();
        for (Lock lock : locks) {
            lockIds.add(lock.getId());
        }
        return lockIds;
    }

    public List<Lock> findAll() { return lockRepository.findAll();}

    public Optional<Lock> findById(Long lockId) { return lockRepository.findById(lockId);}

    public Lock save(Lock lock) { return lockRepository.save(lock);}

}
