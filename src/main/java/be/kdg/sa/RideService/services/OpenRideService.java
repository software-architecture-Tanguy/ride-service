package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.model.Ride;
import be.kdg.sa.RideService.model.Vehicle;
import be.kdg.sa.RideService.model.messages.LocationMessage;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
public class OpenRideService {
    private final RideService rideService;
    private final LocationMessageService locationMessageService;
    private final GeometryFactory geometryFactory;
    @Value("${openRideTimeHours}")
    private int openRideTimeHours;
    @Value("${openRideLocationMinutes}")
    private int openRideLocationMinutes;
    @Value("${locationRange}")
    private double locationRange;

    public OpenRideService(RideService rideService, LocationMessageService locationMessageService, GeometryFactory geometryFactory) {
        this.rideService = rideService;
        this.locationMessageService = locationMessageService;
        this.geometryFactory = geometryFactory;
    }


    @Scheduled(fixedDelay = 60000L)
    private void checkOpenRides() {
        checkOpenRidesByTime();
        checkOpenRidesLocation();
    }


    public boolean checkOpenRidesByTime() {
        boolean openRideFound = false;
        List<Ride> allRides = rideService.findAllUnendedRides();
        for (Ride ride : allRides) {
            if (!checkOpenRideTime(ride)) {
                log.info("An open ride has been detected: " + ride.toString());
                openRideFound = true;
            }
        }
        return openRideFound;
    }


    public boolean checkOpenRidesLocation() {
        boolean openRideLocationFound = false;
        List<Ride> rides = rideService.findAllUnendedRides();
        for (Ride ride : rides) {
            if (!checkOpenRideLocation(ride)) {
                checkOpenRideLocation(ride);
                log.info("=============An open ride has been detected!=============" + ride.toString());
                openRideLocationFound = true;
            }
        }
        return openRideLocationFound;
    }

    private Boolean checkOpenRideTime(Ride ride) {
        return !ride.getStartTime().isBefore(LocalDateTime.now().minusHours(openRideTimeHours));
    }

    private Boolean checkOpenRideLocation(Ride ride) {
        boolean openRideFound = false;
        List<LocationMessage> locationMessagesForRide = locationMessageService.findAllLocationMessagesForRide(ride.getVehicleId(), ride.getStartTime());
        if (!(locationMessagesForRide.size() == 0)) {
            LocationMessage lastLocationMessage = locationMessagesForRide.get(locationMessagesForRide.size() - 1);
            Point lastLocationPoint = geometryFactory.createPoint(new Coordinate(lastLocationMessage.getXCoord(), lastLocationMessage.getYCoord()));
            for (LocationMessage message : locationMessagesForRide) {
                Point point = geometryFactory.createPoint(new Coordinate(message.getXCoord(), message.getYCoord()));
                if (lastLocationMessage.getTimestamp().minusMinutes(openRideLocationMinutes).isAfter(message.getTimestamp()) && point.distance(lastLocationPoint)< locationRange) {
                    openRideFound = true;
                }
            }
        }
        return openRideFound;
    }

}
