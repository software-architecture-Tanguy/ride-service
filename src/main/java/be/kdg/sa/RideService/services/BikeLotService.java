package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.model.Bikelot;
import be.kdg.sa.RideService.persistence.BikeLotRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@AllArgsConstructor
@Service
public class BikeLotService {
    private final BikeLotRepository bikeLotRepository;

    public Optional<Bikelot> load(Long id) {
        return bikeLotRepository.findById(id);
    }

}
