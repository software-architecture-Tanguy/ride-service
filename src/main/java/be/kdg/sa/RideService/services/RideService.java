package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.model.Ride;
import be.kdg.sa.RideService.model.Subscription;
import be.kdg.sa.RideService.persistence.RideRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
@Transactional
public class RideService {

    private final RideRepository rideRepository;
    private final LockService lockService;
    private final StationService stationService;
    private final SubscriptionService subscriptionService;


    public Ride save(Ride ride) {
        return rideRepository.save(ride);
    }

    public Optional<Ride> load(long id) {
        return rideRepository.findById(id);
    }

    public List<Ride> findAll(){
        return rideRepository.findAll();
    }

    public Ride findLatestRideOfUser(Long subscriptionId) { return rideRepository.findFirstBySubscriptionIdAndEndTimeIsNullOrderByStartTimeDesc(subscriptionId);}

    public Ride findLatestCompletedRideByUser(Long subcriptionid) { return rideRepository.findFirstBySubscriptionIdAndEndTimeIsNotNullOrderByEndTime(subcriptionid);}

    public List<Ride> findAllUnendedRides() { return rideRepository.findAllByEndTimeIsNull();}
}
