package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.model.messages.LocationMessage;
import be.kdg.sa.RideService.persistence.LocationMessageRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@AllArgsConstructor
public class LocationMessageService {
    private final LocationMessageRepository locationMessageRepository;

    public LocationMessage locationMessageByVehicleId(Long vehicleId) {
        return locationMessageRepository.findByVehicleId(vehicleId);
    }

    public List<LocationMessage> findAllLocationMessagesForRide(Long vehicleId, LocalDateTime start) { return locationMessageRepository.findAllByVehicleIdAndTimestampIsAfter(vehicleId, start);}

    public LocationMessage save(LocationMessage locationMessage) {
        return locationMessageRepository.save(locationMessage);
    }
}
