package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.exceptions.VehicleNotFoundException;
import be.kdg.sa.RideService.model.Vehicle;
import be.kdg.sa.RideService.model.dto.UpdateVehicleDTO;
import be.kdg.sa.RideService.persistence.VehicleRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;


@Transactional
@Service
@AllArgsConstructor
@Slf4j
public class VehicleService {
    private final VehicleRepository vehicleRepository;

    public List<Vehicle> findAllVehicles() { return vehicleRepository.findAll();}

    public Vehicle save(Vehicle vehicle) { return vehicleRepository.save(vehicle);}

    public Vehicle load(Long id) throws VehicleNotFoundException {
        Optional<Vehicle> vehicle = vehicleRepository.findById(id);
        if (vehicle.isPresent()) {
            return vehicle.get();
        }
        else {
            throw new VehicleNotFoundException("Unable to find vehicle!");
        }
    }

    public Vehicle updateVehicle(long id, UpdateVehicleDTO updateVehicleDTO) throws VehicleNotFoundException {
        Vehicle vehicle = load(id);
        vehicle.setSerialNumber(updateVehicleDTO.getSerialNumber());
        vehicle.setBikeLotId(updateVehicleDTO.getBikeLotId());
        vehicle.setLastMaintenanceOn(updateVehicleDTO.getLastMaintenanceOn());
        vehicle.setLockId(updateVehicleDTO.getLockId());
        vehicle.setPoint(updateVehicleDTO.getPoint());
        return save(vehicle);
    }

    public void delete(Long id) throws VehicleNotFoundException {
        Optional<Vehicle> vehicle = vehicleRepository.findById(id);
        if (vehicle.isPresent()) {
            vehicleRepository.deleteById(id);
        }
        else {
            throw new VehicleNotFoundException("Vehicle wasn't found!");
        }
    }

}
