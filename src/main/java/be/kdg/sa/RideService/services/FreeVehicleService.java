package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.exceptions.VehicleNotFoundException;
import be.kdg.sa.RideService.model.Ride;
import be.kdg.sa.RideService.model.Subscription;
import be.kdg.sa.RideService.model.Vehicle;
import com.vividsolutions.jts.geom.Point;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class FreeVehicleService {

    private final SubscriptionService subscriptionService;
    private final VehicleService vehicleService;
    private final RideService rideService;

    public Boolean unlockFreeVehicle(Long userid, Long vehicleId) throws VehicleNotFoundException {
        List<Subscription> subscriptionsByUserid = subscriptionService.findAllByUserId(userid);
        Subscription subscription = subscriptionsByUserid.stream().findFirst().get();
        Vehicle vehicle = vehicleService.load(vehicleId);
        Ride ride = new Ride(vehicle.getPoint(), LocalDateTime.now(), vehicleId, subscription.getSubscriptionId());
        rideService.save(ride);
        return true;
    }

    public Boolean lockFreeVehicle(Long vehicleId, Long userid) throws VehicleNotFoundException {
        Vehicle vehicle = vehicleService.load(vehicleId);
        List<Subscription> subscriptionsByUserid = subscriptionService.findAllByUserId(userid);
        Subscription subscription = subscriptionsByUserid.stream().findFirst().get();
        Ride ride = rideService.findLatestRideOfUser(subscription.getSubscriptionId());
        ride.setEndTime(LocalDateTime.now());
        ride.setEndPoint(vehicle.getPoint());
        rideService.save(ride);
        return true;
    }

    public Vehicle findNearest(Long vehicleId) throws VehicleNotFoundException {
        Point checkPoint = vehicleService.load(vehicleId).getPoint();
        List<Vehicle> vehicleList = vehicleService.findAllVehicles();
        vehicleList.remove(vehicleId);
        //if (!vehicleList.isEmpty()){
        Vehicle nearestVehicle = new Vehicle();
        Double nearestDistance = 999999999.0;
        for (Vehicle v : vehicleList) {
            Double distance = v.getPoint().distance(checkPoint);
            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestVehicle = v;
            }
        }
        return nearestVehicle;
        //}
        //Throw new Exception("No other vehicles nearby");
    }

}
