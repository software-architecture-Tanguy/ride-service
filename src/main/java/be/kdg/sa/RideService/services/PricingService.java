package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.exceptions.VehicleNotFoundException;
import be.kdg.sa.RideService.messaging.QueueMessenger;
import be.kdg.sa.RideService.model.Bikelot;
import be.kdg.sa.RideService.model.Ride;
import be.kdg.sa.RideService.model.Subscription;
import be.kdg.sa.RideService.model.Vehicle;
import be.kdg.sa.RideService.model.dto.InvoicingDTO;
import be.kdg.sa.priceservice.PriceInfo;
import be.kdg.sa.priceservice.Proxy;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
@AllArgsConstructor
@Slf4j
public class PricingService {
    private final Proxy pricingProxy;
    private final SubscriptionService subscriptionService;
    private final VehicleService vehicleService;
    private final BikeLotService bikeLotService;
    private final RideService rideService;
    private QueueMessenger queueMessenger;

    @Cacheable("pricinginfo")
    public PriceInfo getPricingInfo(int subscriptionType, int vehicleType) throws IOException {
        return pricingProxy.get(subscriptionType, vehicleType);
    }

    public double calculatePrice(Long userid, Long vehicleId) throws VehicleNotFoundException, IOException {
        Subscription subscription = subscriptionService.findAllByUserId(userid).stream().findFirst().get();
        Vehicle vehicle = vehicleService.load(vehicleId);
        Bikelot bikelot = getBikeLot(vehicle.getBikeLotId());
        Ride ride = rideService.findLatestCompletedRideByUser(subscription.getSubscriptionId());
        PriceInfo priceInfo = getPricingInfo(Math.toIntExact(subscription.getSubscriptionTypeId() - 1), Math.toIntExact(bikelot.getBikeTypeId() - 1));
        Long minutes = ChronoUnit.MINUTES.between(ride.getStartTime(), ride.getEndTime());
        int payingMinutes = (int) (minutes - priceInfo.getFreeMinutes());
        if (payingMinutes < 0) {
            payingMinutes = 0;
        }
        return payingMinutes * priceInfo.getCentsPerMinute();
    }

    private Bikelot getBikeLot(Long bikelotId) throws VehicleNotFoundException {
        Bikelot bikelot = null;
        Optional<Bikelot> optionalBikelot = bikeLotService.load(bikelotId);
        if (optionalBikelot.isPresent()) {
            bikelot = optionalBikelot.get();
        } else {
            throw new VehicleNotFoundException("Unable to find vehicle");
        }
        return bikelot;
    }

    public void handleFreeRideInvoices(Long userid, Long vehicleId) throws VehicleNotFoundException, IOException {
        double price = calculatePrice(userid, vehicleId);
        queueMessenger.sendMessage(new InvoicingDTO(userid, price));
    }

    public void handleStationInvoices(Long userid) throws VehicleNotFoundException, IOException {
        Long vehicleId = getVehicleId(userid);
        double price =  calculatePrice(userid, vehicleId);
        queueMessenger.sendMessage(new InvoicingDTO(userid, price));
    }

    private Long getVehicleId(Long userid) {
        Subscription subscription = subscriptionService.findAllByUserId(userid).stream().findFirst().get();
        Ride ride = rideService.findLatestRideOfUser(subscription.getSubscriptionId());
        return ride.getVehicleId();
    }
}
