package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.model.Lock;
import be.kdg.sa.RideService.model.Station;
import be.kdg.sa.RideService.persistence.StationRepository;
import javassist.NotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@Transactional
public class StationService {


    private final StationRepository stationRepository;

    public StationService(StationRepository stationRepository) {
        this.stationRepository = stationRepository;
    }

    public Station save(Station stations) {
        return stationRepository.save(stations);
    }

    public Station load(Long id) {
        return stationRepository.findByStationId(id);
    }

}
