package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.model.Subscription;
import be.kdg.sa.RideService.persistence.SubscriptionRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class SubscriptionService {
    private final SubscriptionRepository subscriptionRepository;

    List<Subscription> findAllByUserId(Long userId) { return subscriptionRepository.findAllBySubscriptionIdOrderByValidFromDesc(userId);}

    Optional<Subscription> load(Long subscriptionId) { return subscriptionRepository.findById(subscriptionId);}
}
