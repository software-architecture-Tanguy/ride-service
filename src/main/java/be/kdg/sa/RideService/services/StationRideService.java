package be.kdg.sa.RideService.services;

import be.kdg.sa.RideService.exceptions.LockNotFoundException;
import be.kdg.sa.RideService.model.Lock;
import be.kdg.sa.RideService.model.Ride;
import be.kdg.sa.RideService.model.Station;
import be.kdg.sa.RideService.model.Subscription;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
@AllArgsConstructor
public class StationRideService {

    private final LockService lockService;
    private final StationService stationService;
    private final SubscriptionService subscriptionService;
    private final RideService rideService;

    public Long unlockStationVehicle(Long userId, Long stationId) {
        Lock lock = null;
        List<Long> freeLocksStation = lockService.findAllFreeLocksByStation(stationId);
        Optional<Lock> optionalLock = lockService.findById(freeLocksStation.stream().findFirst().get());
        if (optionalLock.isPresent()) {
            lock = optionalLock.get();
        }
        Station station = stationService.load(stationId);
        List<Subscription> subscriptionsByUserid = subscriptionService.findAllByUserId(userId);
        Subscription subscription = subscriptionsByUserid.stream().findFirst().get();
        Ride ride = new Ride(station.getGPSCoord(), LocalDateTime.now(), lock.getVehicleId(), subscription.getSubscriptionId(), lock.getId());
        rideService.save(ride);
        return lock.getId();
    }


    public void lockStationVehicle(Long userid, Long lockId) throws LockNotFoundException {
        List<Subscription> subscriptionsByUserid = subscriptionService.findAllByUserId(userid);
        Subscription subscription = subscriptionsByUserid.stream().findFirst().get();
        Ride ride = rideService.findLatestRideOfUser(subscription.getSubscriptionId());
        Optional<Lock> optionalLock = lockService.findById(lockId);
        if (optionalLock.isPresent()) {
            Lock lock = optionalLock.get();
            Station station = stationService.load(lock.getStationId());
            ride.setEndPoint(station.getGPSCoord());
            lock.setVehicleId(ride.getVehicleId());
            lockService.save(lock);
        } else throw new LockNotFoundException("Unable to find the lock!");
        ride.setEndTime(LocalDateTime.now());
        ride.setEndLockId(lockId);
        rideService.save(ride);
    }
}
