package be.kdg.sa.RideService.messaging;

import be.kdg.sa.RideService.model.dto.InvoicingDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class QueueMessenger {

    public QueueMessenger(RabbitTemplate rabbitTemplate, Queue queue) {
        this.rabbitTemplate = rabbitTemplate;
        this.queue = queue;
    }

    private final RabbitTemplate rabbitTemplate;
    private final Queue queue;
    private XmlMapper xmlMapper = new XmlMapper();


    public void sendMessage(InvoicingDTO invoicingDTO) {
        try {
            this.rabbitTemplate.convertAndSend(queue.getName(), xmlMapper.writeValueAsString(invoicingDTO));
            log.info("An invoice was sent to the queue");
        } catch (AmqpException | JsonProcessingException e) {
            log.info("Unable to connect to the queue!");
        }
    }
}


