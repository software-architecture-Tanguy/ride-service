package be.kdg.sa.RideService.messaging;

import be.kdg.sa.RideService.mapping.JSONService;
import be.kdg.sa.RideService.model.messages.LocationMessage;
import be.kdg.sa.RideService.services.LocationMessageService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.retry.support.RetryTemplate;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "\"location\"")
@AllArgsConstructor
@Slf4j
public class LocationMessageReceiver {

    private final RetryTemplate retryTemplate;
    private final JSONService jsonService;
    private final LocationMessageService locationMessageService;

    @RabbitHandler
    public void receiveMessage(String message) {
        retryTemplate.execute(arg0 -> {
            LocationMessage locationMessage = (LocationMessage) jsonService.convertJSONToObject(message, LocationMessage.class);
            if (locationMessage == null) {
                try {
                    Thread.sleep(1000);
                    log.info("No messages found, idling");
                } catch (InterruptedException e) {
                    log.warn("The queue has been interrupted.");
                }
            } else {
                try {
                    locationMessageService.save(locationMessage);
                    log.info("A message was received from the queue" + locationMessage.toString());
                } catch (Exception e) {
                    log.warn("Unable to process message!");
                }
            }
            return null;
        });
    }


}
