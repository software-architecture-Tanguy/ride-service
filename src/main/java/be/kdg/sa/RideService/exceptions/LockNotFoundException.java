package be.kdg.sa.RideService.exceptions;

public class LockNotFoundException extends Exception {
    public LockNotFoundException(String message) {
        super(message);
    }
}
