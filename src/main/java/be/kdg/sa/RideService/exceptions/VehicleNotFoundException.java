package be.kdg.sa.RideService.exceptions;

public class VehicleNotFoundException extends Exception {
    public VehicleNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
