package be.kdg.sa.RideService.config;

import be.kdg.sa.priceservice.Proxy;
import com.bedatadriven.jackson.datatype.jts.JtsModule;
import com.vividsolutions.jts.geom.GeometryFactory;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebConfig {

    @Bean
    public Proxy proxy() {
        return new Proxy();
    }

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public JtsModule jtsModule() {
        return new JtsModule();
    }

    @Bean
    public GeometryFactory geometryFactory() { return new GeometryFactory();}

}
