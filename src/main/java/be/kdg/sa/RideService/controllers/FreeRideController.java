package be.kdg.sa.RideService.controllers;

import be.kdg.sa.RideService.exceptions.VehicleNotFoundException;
import be.kdg.sa.RideService.model.Vehicle;
import be.kdg.sa.RideService.services.FreeVehicleService;
import be.kdg.sa.RideService.services.PricingService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/free")
@AllArgsConstructor
public class FreeRideController {

    private final FreeVehicleService freeVehicleService;
    private final PricingService pricingService;

    @GetMapping(value = "/unlock/{userid}/{vehicleid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Boolean unlockFreeVehicle(@PathVariable Long userid, @PathVariable Long vehicleid) throws VehicleNotFoundException {
        return freeVehicleService.unlockFreeVehicle(userid, vehicleid);
    }

    @GetMapping(value = "/lock/{userid}/{vehicleid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> lockFreeVehicle(@PathVariable Long userid, @PathVariable Long vehicleid) throws VehicleNotFoundException, IOException {
        pricingService.handleFreeRideInvoices(userid, vehicleid);
        return  new ResponseEntity<>(freeVehicleService.lockFreeVehicle(vehicleid, userid), HttpStatus.OK);
    }

    @GetMapping("/nearest/{vehicleid}")
    public Long findNearest(@PathVariable Long vehicleid) throws VehicleNotFoundException {
        Vehicle nearestVehicle =  freeVehicleService.findNearest(vehicleid);
        return nearestVehicle.getVehicleId();
    }
}
