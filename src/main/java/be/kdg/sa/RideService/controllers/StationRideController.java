package be.kdg.sa.RideService.controllers;

import be.kdg.sa.RideService.exceptions.LockNotFoundException;
import be.kdg.sa.RideService.exceptions.VehicleNotFoundException;
import be.kdg.sa.RideService.services.PricingService;
import be.kdg.sa.RideService.services.StationRideService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("/station")
@AllArgsConstructor
public class StationRideController {

    private final StationRideService stationRideService;
    private final PricingService pricingService;

    @GetMapping(value = "/unlock/{userid}/{stationid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Long unlockStationVehicle(@PathVariable Long userid, @PathVariable Long stationid) {
        return stationRideService.unlockStationVehicle(userid, stationid);
    }

    @PutMapping(value = "/lock/{userid}/{lockid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void lockStationVehicle(@PathVariable Long userid, @PathVariable Long lockid) throws LockNotFoundException, VehicleNotFoundException, IOException {
        pricingService.handleStationInvoices(userid);
        stationRideService.lockStationVehicle(userid, lockid);
    }


}
