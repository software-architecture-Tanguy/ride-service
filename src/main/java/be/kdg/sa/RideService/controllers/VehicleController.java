package be.kdg.sa.RideService.controllers;

import be.kdg.sa.RideService.exceptions.VehicleNotFoundException;
import be.kdg.sa.RideService.model.Vehicle;
import be.kdg.sa.RideService.model.dto.UpdateVehicleDTO;
import be.kdg.sa.RideService.model.dto.VehicleDTO;
import be.kdg.sa.RideService.services.VehicleService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/vehicle")
@AllArgsConstructor
public class VehicleController {

    private final VehicleService vehicleService;
    private final ModelMapper modelMapper;

    @GetMapping("/load/{id}")
    public ResponseEntity<VehicleDTO> loadVehicle(@PathVariable(value = "id") Long id) throws VehicleNotFoundException {
        Vehicle vehicle = vehicleService.load(id);
        return new ResponseEntity<>(modelMapper.map(vehicle, VehicleDTO.class), HttpStatus.OK);
    }

    @RequestMapping(value = "delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Vehicle> deleteVehicle(@PathVariable Long id) throws VehicleNotFoundException {
        vehicleService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<UpdateVehicleDTO> updateVehicle(@PathVariable Long id, @Valid @RequestBody UpdateVehicleDTO updateVehicleDTO) throws VehicleNotFoundException {
        Vehicle vehicle = vehicleService.updateVehicle(id, updateVehicleDTO);
        return new ResponseEntity<>(modelMapper.map(vehicle, UpdateVehicleDTO.class), HttpStatus.ACCEPTED);
    }

    @PostMapping("/create")
    public ResponseEntity<VehicleDTO> createVehicle(@Valid @RequestBody VehicleDTO vehicleDTO) {
        vehicleService.save(modelMapper.map(vehicleDTO, Vehicle.class));
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
