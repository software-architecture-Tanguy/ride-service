package be.kdg.sa.RideService.controllers;

import be.kdg.sa.RideService.services.LockService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/lock")
@AllArgsConstructor
public class LockController {


    private final LockService lockService;
    
    @GetMapping(value = "/{stationID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Long>> getFreeLocks(@PathVariable Long stationID) {
        List<Long> freeLocks = lockService.findAllFreeLocksByStation(stationID);
        return new ResponseEntity<>(freeLocks, HttpStatus.OK);
    }
}
