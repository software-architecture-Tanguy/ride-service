package be.kdg.sa.RideService.persistence;

import be.kdg.sa.RideService.model.Ride;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RideRepository  extends JpaRepository<Ride,Long>  {
    Ride findFirstBySubscriptionIdAndEndTimeIsNullOrderByStartTimeDesc(Long subscriptionId);

    Ride findFirstBySubscriptionIdAndEndTimeIsNotNullOrderByEndTime(Long subscriptionId);

    List<Ride> findAllByEndTimeIsNull();
}
