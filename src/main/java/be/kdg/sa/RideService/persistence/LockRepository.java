package be.kdg.sa.RideService.persistence;

import be.kdg.sa.RideService.model.Lock;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LockRepository extends JpaRepository<Lock, Long> {
    List<Lock> findAllByStationIdAndVehicleIdIsNotNull(Long stationId);

}
