package be.kdg.sa.RideService.persistence;


import be.kdg.sa.RideService.model.Station;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface StationRepository extends JpaRepository<Station, String> {
    Station findByStationId(Long id);

}
