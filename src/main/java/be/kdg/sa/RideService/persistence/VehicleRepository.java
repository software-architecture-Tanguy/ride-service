package be.kdg.sa.RideService.persistence;

import be.kdg.sa.RideService.model.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VehicleRepository extends JpaRepository<Vehicle, Long> {
    List<Vehicle> findAll();
}
