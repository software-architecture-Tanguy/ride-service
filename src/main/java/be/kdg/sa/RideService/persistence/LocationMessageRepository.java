package be.kdg.sa.RideService.persistence;

import be.kdg.sa.RideService.model.messages.LocationMessage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;

public interface LocationMessageRepository extends JpaRepository<LocationMessage, Long> {
    LocationMessage findByVehicleId(Long vehicleId);

    List<LocationMessage> findAllByVehicleIdAndTimestampIsAfter(Long vehicleId, LocalDateTime start);
}
