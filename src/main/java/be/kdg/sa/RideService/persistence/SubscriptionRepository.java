package be.kdg.sa.RideService.persistence;

import be.kdg.sa.RideService.model.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
    List<Subscription> findAllBySubscriptionIdOrderByValidFromDesc(Long id);
}
