package be.kdg.sa.RideService.persistence;

import be.kdg.sa.RideService.model.Bikelot;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BikeLotRepository extends JpaRepository<Bikelot, Long> {
}
