package be.kdg.sa.RideService.model.messages;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@NoArgsConstructor
@Data
@Entity
@IdClass(LocationMessage.class)
@Table(name = "LocationMessages")
public class LocationMessage implements Serializable {
    @Id
    @Column(name = "VehicleId", columnDefinition="smallint")
    private Long vehicleId;
    @Column(name = "xCoord", columnDefinition="varchar(50)")
    private double xCoord;
    @Column(name = "yCoord", columnDefinition="varchar(50)")
    private double yCoord;
    @Id
    @Column(name = "timestamp", columnDefinition = "datetime")
    private LocalDateTime timestamp;

    public LocationMessage(long l, double v, double v1, LocalDateTime now) {
        this.vehicleId = l;
        this.xCoord = v;
        this.yCoord = v1;
        this.timestamp = now;
    }
}
