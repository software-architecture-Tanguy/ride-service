package be.kdg.sa.RideService.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

//TODO Conform DTO's
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RideDTO {
    private long  id;
    private String  startPoint;
    private String  endPoint;
    private LocalDateTime startTime;
    private LocalDateTime  endTime;
    private long  vehicleId;
    private String  subscriptionId;
    private String  employeeId;
    private String  startLockId;
    private String  endLockId;
    private String  maintenanceTypeId;
}
