package be.kdg.sa.RideService.model;


import com.vividsolutions.jts.geom.Point;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "Stations")
public class Station {

    @Id
    @Column(name = "StationId", columnDefinition = "smallint", nullable = false)
    private Long stationId;

    @Column(name = "ObjectId", columnDefinition = "nvarchar(20)", nullable = false)
    private String objectId;

    @Column(name = "StationNr", columnDefinition = "nvarchar(20)", nullable = false)
    private String stationNr;
    @Column(name = "Type", columnDefinition = "nvarchar(20)", nullable = false)
    private String type;
    @Column(name = "Street", columnDefinition = "nvarchar(100)", nullable = false)
    private String street;

    @Column(name = "Number", columnDefinition = "nvarchar(10)")
    private String number;
    @Column(name = "ZipCode", columnDefinition = "nvarchar(10)", nullable = false)
    private String zipCode;
    @Column(name = "District", columnDefinition = "nvarchar(100)", nullable = false)
    private String district;
    @Column(name = "GPSCoord", columnDefinition = "geometry", nullable = false)
    private Point GPSCoord;
    @Column(name = "AdditionalInfo", columnDefinition = "nvarchar(100)", nullable = false)
    private String additionalInfo;
    @Column(name = "LabelId", columnDefinition = "tinyint", nullable = false)
    private Integer labelId;
    @Column(name = "CityId", columnDefinition = "tinyint", nullable = false)
    private Integer cityId;


}
