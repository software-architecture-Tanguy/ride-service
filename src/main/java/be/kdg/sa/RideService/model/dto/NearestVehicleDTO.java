package be.kdg.sa.RideService.model.dto;

import com.vividsolutions.jts.geom.Point;
import lombok.Data;

//TODO Conform DTO's
@Data
public class NearestVehicleDTO {
    private Long vehicleId;
    private Point point;
}
