package be.kdg.sa.RideService.model;

import com.vividsolutions.jts.geom.Point;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@NoArgsConstructor
@Table(name = "Vehicles")
public class Vehicle {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "VehicleId", columnDefinition = "smallint", nullable = false)
    private Long vehicleId;
    @Column(name = "SerialNumber", columnDefinition = "nvarchar(20)", nullable = false)
    private String serialNumber;
//    @ManyToOne
    @Column(name = "BikeLotId", columnDefinition = "smallint", nullable = false)
    private Long bikeLotId;
    @Column(name = "LastMaintenanceOn", columnDefinition = "datetime")
    private LocalDateTime lastMaintenanceOn;
    @Column(name = "LockId", columnDefinition = "smallint")
    private Long lockId;
    @Column(name = "Point", columnDefinition = "geometry")
    private Point point;

    public Vehicle(String serialNumber, Long bikeLotId, LocalDateTime lastMaintenanceOn, Long lockId, Point point) {
        this.serialNumber = serialNumber;
        this.bikeLotId = bikeLotId;
        this.lastMaintenanceOn = lastMaintenanceOn;
        this.lockId = lockId;
        this.point = point;
    }
}
