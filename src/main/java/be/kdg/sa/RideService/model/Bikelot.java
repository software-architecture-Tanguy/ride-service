package be.kdg.sa.RideService.model;


import lombok.Data;


import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "Bikelots")
public class Bikelot {

    @Id
    @Column(name = "BikeLotId", columnDefinition="smallint", nullable = false)
    private Long  id;

    @Column(name = "DeliveryDate", columnDefinition="date")
    private LocalDateTime deliveryDate;

//    @OneToMany
    @Column(name = "BikeTypeId", columnDefinition="tinyint")
    private Long bikeTypeId;

}
