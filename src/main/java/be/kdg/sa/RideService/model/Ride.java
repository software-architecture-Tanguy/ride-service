package be.kdg.sa.RideService.model;

import com.vividsolutions.jts.geom.Point;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Rides")
@Data
@NoArgsConstructor
public class Ride {

    @Id
    @Column(name = "RideId", columnDefinition="bigint", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long  id;
    @Column(name = "StartPoint", columnDefinition="geometry")
    private Point startPoint;
    @Column(name = "EndPoint", columnDefinition="geometry")
    private Point  endPoint;
    @Column(name = "StartTime", columnDefinition="datetime")
    private LocalDateTime startTime;
    @Column(name = "EndTime", columnDefinition="datetime")
    private LocalDateTime  endTime;
    @Column(name = "VehicleId", columnDefinition="smallint", nullable = false)
    private Long  vehicleId;
    @Column(name = "SubscriptionId", columnDefinition="int")
    private Long  subscriptionId;
    @Column(name = "EmployeeId", columnDefinition="smallint")
    private String  employeeId;
    @Column(name = "StartLockId", columnDefinition="smallint")
    private Long  startLockId;
    @Column(name = "EndLockId", columnDefinition="smallint")
    private Long  endLockId;
    @Column(name = "MaintenanceTypeId", columnDefinition="tinyint")
    private Long  maintenanceTypeId;

    public Ride(Point startPoint, LocalDateTime startTime, long vehicleId, Long subscriptionId, Long startLockId) {
        this.startPoint = startPoint;
        this.startTime = startTime;
        this.vehicleId = vehicleId;
        this.subscriptionId = subscriptionId;
        this.startLockId = startLockId;
    }

    public Ride(Point point, LocalDateTime now, Long vehicleId, Long subscriptionId) {
        this.startPoint = point;
        this.startTime = now;
        this.vehicleId = vehicleId;
        this.subscriptionId = subscriptionId;
    }

    public Ride(LocalDateTime localDateTime, long vehicleId, long subscriptionId) {
        this.startTime = localDateTime;
        this.vehicleId =vehicleId;
        this.subscriptionId = subscriptionId;
    }
}
