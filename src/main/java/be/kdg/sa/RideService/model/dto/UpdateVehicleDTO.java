package be.kdg.sa.RideService.model.dto;

import com.vividsolutions.jts.geom.Point;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

//TODO Conform DTO's
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateVehicleDTO {
    private String serialNumber;
    private Long bikeLotId;
    private LocalDateTime lastMaintenanceOn;
    private Long lockId;
    private Point point;


}
