package be.kdg.sa.RideService.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class User {
    @Id
    @Column(name = "UserId", columnDefinition="int", nullable = false)
    private Long  subscriptionTypeId;
    @Column(name = "Name", columnDefinition = "nvarchar(100)")
    private String name;
    @Column(name = "Email", columnDefinition = "nvarchar(100)", nullable = false)
    private String email;

    @Column(name = "Street", columnDefinition = "nvarchar(100)")
    private String street;

    @Column(name = "Number", columnDefinition = "nvarchar(10)")
    private Integer number;

    @Column(name = "ZipCode", columnDefinition = "nvarchar(10)")
    private Integer zipCode;

    @Column(name = "City", columnDefinition = "nvarchar(100)")
    private String city;
    @Column(name = "CountryCode", columnDefinition = "nvarchar(3)")
    private String countryCode;

}
