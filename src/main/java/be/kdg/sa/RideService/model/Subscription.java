package be.kdg.sa.RideService.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "Subscriptions")
@Data
public class Subscription {

    @Id
    @Column(name = "SubscriptionId", columnDefinition="int", nullable = false)
    private Long  subscriptionId;
    @Column(name = "ValidFrom", columnDefinition="date", nullable = false)
    private LocalDateTime validFrom;
//    @ManyToOne
    @Column(name = "SubscriptionTypeId", columnDefinition="tinyint", nullable = false)
    private Long  subscriptionTypeId;
//    @ManyToOne
    @Column(name = "UserId", columnDefinition="int", nullable = false)
    private Long  userId;

}
