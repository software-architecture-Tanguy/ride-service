package be.kdg.sa.RideService.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class InvoicingDTO {
    private Long userid;
    private double amount;
}
