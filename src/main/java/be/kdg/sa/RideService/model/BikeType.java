package be.kdg.sa.RideService.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BikeTypes")
public class BikeType {
    @Id
    @Column(name = "BikeTypeId", columnDefinition="tinyint", nullable = false)
    private Long  id;

    @Column(name = "BikeTypeDescription", columnDefinition="nvarchar(200)")
    private String description;
}
