package be.kdg.sa.RideService.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@ToString
@Data
@Table(name = "Locks")
public class Lock {

    @Id
    @Column(name = "LockId", columnDefinition="smallint", nullable = false)
    private Long  id;
    @Column(name = "StationLockNr", columnDefinition="tinyint", nullable = false)
    private Long  stationLockNr;
//    @ManyToOne
    @Column(name = "StationId", columnDefinition="smallint", nullable = false)
    private Long  stationId;
//    @OneToOne

    @Column(name = "VehicleId", columnDefinition="smallint", nullable = true)
    private Long  vehicleId;

}
