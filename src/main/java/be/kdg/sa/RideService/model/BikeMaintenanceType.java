package be.kdg.sa.RideService.model;


import lombok.Data;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BikeMaintenanceTypes")
public class BikeMaintenanceType {

    @Id
    @Column(name = "BikeMaintenanceTypeId", columnDefinition="tinyint", nullable = false)
    private Long  id;

    @Column(name = "Description", columnDefinition="nvarchar(50)")
    private String description;

}
