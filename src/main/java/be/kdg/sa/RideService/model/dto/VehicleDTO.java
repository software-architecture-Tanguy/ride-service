package be.kdg.sa.RideService.model.dto;

import com.vividsolutions.jts.geom.Point;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

//TODO Conform DTO's
@AllArgsConstructor
@NoArgsConstructor
@Data
public class VehicleDTO {
    private Long vehicleId;
    private String serialNumber;
    private Long bikeLotId;
    private LocalDateTime lastMaintenanceOn;
    private Long lockId;
    private Point point;
}
