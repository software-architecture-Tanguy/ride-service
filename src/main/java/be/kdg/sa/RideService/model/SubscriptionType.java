package be.kdg.sa.RideService.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SubscriptionTypes")
public class SubscriptionType {
    @Id
    @Column(name = "SubscriptionTypeId", columnDefinition="tinyint", nullable = false)
    private Long  subscriptionTypeId;
    @Column(name = "Description", columnDefinition="nvarchar(50)", nullable = false)
    private String  description;

}
